# Python Ryzom Libs

This is a set of useful classes used in the leveldesign and utilities of the Ryzom MMORPG.

## Getting started

First, create your python virtual env or use one of them.

Use the pip tool from your virtual env
```pip install 'git+https://gitlab.com/ryzom/python-ryzom-libs.git#egg=ryzom-python-libs[DIST]' ```