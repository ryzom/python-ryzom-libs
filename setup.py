import os
import configparser
from setuptools import setup


config = configparser.ConfigParser()
config.read("setup.ini")
print("--------------", config._sections["options"])
if __name__ == "__main__":
	package = config["options"]["packages"].split("\n")[1]
	setup(
		name=config["metadata"]["name"],
		description=config["metadata"]["description"],
		url=config["metadata"]["url"],
		version=config["metadata"]["version"],
		author=config["author"]["name"]+" ("+config["author"]["email"]+")",
		long_description="file: README.md",
		keywords=config["metadata"]["keywords"],
		python_requires=">=3.8",
		package_dir={package:package},
		packages=[package],
		install_requires=config["options"]["install_requires"],
	)
